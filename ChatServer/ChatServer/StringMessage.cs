﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer
{
    [Serializable]
    class StringMessage
    {
        public string Message { get; private set; }
        public StringMessage(string message)
        {
            this.Message = message;
        }
    }
}
