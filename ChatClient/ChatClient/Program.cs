﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;

namespace ChatClient
{
    class Program
    {
        static string userName;
        private const string host = "127.0.0.1";
        private const int port = 13000;
        static TcpClient client;
        static NetworkStream stream;
        static void Main(string[] args)
        {
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            client = new TcpClient();
            try
            {
                client.Connect(host, port); //подключение клиента
                stream = client.GetStream(); // получаем поток

                string message = userName;
                StringMessage sm = new StringMessage(message);
                string sJSON = Newtonsoft.Json.JsonConvert.SerializeObject(sm);
                byte[] data = Encoding.Unicode.GetBytes(sJSON);
                stream.Write(data, 0, data.Length);

                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //старт потока
                Console.WriteLine("Добро пожаловать, {0}", userName);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
        // отправка сообщений
        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");

            while (true)
            {
                string message = Console.ReadLine();
                StringMessage sm = new StringMessage(message);
                string sJSON = Newtonsoft.Json.JsonConvert.SerializeObject(sm);
                byte[] data = Encoding.Unicode.GetBytes(sJSON);
                stream.Write(data, 0, data.Length);
            }
        }
        // получение сообщений
        static void ReceiveMessage()
        {
            StringMessage sm;
            while (true)
            {
                try
                {
                    byte[] data = new byte[64]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    string sJSON = "";
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        try
                        {
                            sJSON = Encoding.Unicode.GetString(data, 0, bytes);
                            sm = Newtonsoft.Json.JsonConvert.DeserializeObject<StringMessage>(sJSON);
                        }
                        catch (Exception ex)
                        {
                            sm = new StringMessage(" Ошибка десериализации");

                        }


                        builder.Append(sm.Message);


                    }
                    while (stream.DataAvailable);

                    string message = builder.ToString();
                    Console.WriteLine(message);//вывод сообщения
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }

    }
}
